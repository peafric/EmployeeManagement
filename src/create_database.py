from employee_management import db
from employee_management.models import Role, Person, Project, Post, Employment
#
from datetime import datetime, timedelta
from random import randint

if __name__ == '__main__':
    db.create_all()
    #
    role_1 = Role(name="worker")
    role_2 = Role(name="manager")
    #
    db.session.add(role_1)
    db.session.add(role_2)
    db.session.commit()    
    #
    person_1 = Person(username="admin",password="pass",name="Petar",email="petar.afric@fer.hr")
    person_2 = Person(username="e1",password="temp1",name="Robert",email="robi@gmail.com")
    person_3 = Person(username="e2",password="temp2",name="Danica",email="danica@gmail.com")
    people = [person_1, person_2, person_3]
    #    
    project = Project(name="Employee Management")
    #
    employment_1 = Employment(person=person_1, project=project, role=role_2)
    employment_1 = Employment(person=person_1, project=project, role=role_1)
    employment_2 = Employment(person=person_2, project=project, role=role_1)
    employment_3 = Employment(person=person_3, project=project, role=role_1)
    #
    db.session.add(person_1)
    db.session.add(person_2)
    db.session.add(person_3)
    db.session.add(project)
    db.session.add(employment_1)
    db.session.add(employment_2)
    db.session.add(employment_3)
    db.session.commit()    
    #
    post_1 = Post(person_id=person_2.id, project_id=project.id, content="Learning Flask", duration=5)
    post_2 = Post(person_id=person_2.id, project_id=project.id, content="Hope this will improve my Web Flask skills!")
    post_3 = Post(person_id=person_2.id, project_id=project.id, content="This actually works.", duration=2)
    post_4 = Post(person_id=person_2.id, project_id=project.id, content="Learning is fun.")
    post_5 = Post(person_id=person_2.id, project_id=project.id, content="Perhaps there is something to this.", duration=1)
    # 
    db.session.add(post_1)
    db.session.add(post_2)
    db.session.add(post_3)
    db.session.add(post_4)
    db.session.add(post_5)
    #
    for i in range(100):
        person = people[randint(0,len(people)-1)]
        date = datetime.now() - timedelta(days=(randint(0,30)))
        #
        post = Post(person_id=person.id, project_id=project.id, content="Learning Flask post: " + str(i), duration=randint(0,24), date=date)
        db.session.add(post)
    #
    db.session.commit()    
