from employee_management import app, db
#
from employee_management.projects.forms import CreateProjectForm, EditProjectForm
from employee_management.projects.reports import generate_report
#
from flask import Blueprint
from flask import render_template, flash, redirect, url_for, send_file
from flask import request
#
from flask_login import current_user, login_required
#
from employee_management.models import Employment, Project, Person, Role
#
import os
from datetime import datetime, timedelta
    
#=============================================================================================================================
projects_domain = Blueprint('projects_domain', __name__)        
#=============================================================================================================================

@projects_domain.route("/projects", methods=['GET', 'POST'])
@login_required
def projects():    
    #Check for mistakes
    create_project_form = CreateProjectForm()
    if create_project_form.validate_on_submit():
        #
        project_name = str(create_project_form.name.data)
        #
        project = Project(name=project_name)
        manager_role = Role.query.filter_by(name='manager').first()
        employment = Employment(person=current_user, project=project, role=manager_role)
        db.session.add(project)
        db.session.add(employment)
        db.session.commit()
        #
        flash("Project "+project_name+" created successfully!", 'success')     

    #Get all people 
    people = Person.query.all()

    #Get all the projects of the current user
    projects = [employment.project for employment in current_user.employments if employment.role.name=="manager"]    
    for project in projects:
        project.time = sum([post.duration for post in project.posts])
        project_people = [person for person in people if person.id not in set([p.person_id for p in project.personnel if p.role.name=='worker'])]
        project_potential_managers = [person for person in people if person.id not in set([p.person_id for p in project.personnel if p.role.name=='manager'])]
        if len(project_people)>0:
            project.people = project_people
            project.managers = project_potential_managers
    
    #    
    return render_template('projects.html', title='Projects', projects=projects, people=people, create_project_form=create_project_form)


@projects_domain.route("/project", methods=['GET'])
@login_required
def project():    
    project_id = int(request.args.get('project_id', -1))
    if project_id == -1:
        flash("Project could not be found",'danger')
        return redirect(url_for('home'))
    #    
    project = Project.query.filter_by(id=project_id).first()
    #
    for person in project.personnel:
        if person.role.name=='worker':
            person.total_hours = sum([post.duration for post in project.posts if post.person_id==person.person.id])
            person.total_posts = sum([1 for post in project.posts if post.person_id==person.person.id])
    #
    day_data_list = []
    if len(project.posts)>0:    
        today = datetime.now()
        day_data = dict()
        for i in range(30):
            day = today - timedelta(days=i)
            #
            key = day.strftime("%d.%m.%Y")
            #
            day_data[key] = dict()
            day_data[key]['hours'] = sum([post.duration for post in project.posts if post.date.date()==day.date()])
            day_data[key]['posts'] = sum([1 for post in project.posts if post.date.date()==day.date()])
            #
        for day_id in day_data:
            day_data_list.append((day_id,day_data[day_id]['hours'],day_data[day_id]['posts']))
        day_data_list.reverse()
    #
    posts = project.posts
    posts.sort(key=lambda p:p.date)
    posts.reverse()
    #
    if len(day_data_list)!=0:
        return render_template('project_details.html', title='Project', project=project, posts=posts, day_data=day_data_list)
    else:
        return render_template('project_details.html', title='Project', project=project, posts=posts)

@projects_domain.route("/edit_project", methods=['GET','POST'])
@login_required
def edit_project():
    project_id = int(request.args.get('project_id'))
    if project_id is not None:
        project = Project.query.filter_by(id=project_id).first()
        #
        edit_project_form = EditProjectForm()
        if edit_project_form.validate_on_submit():
            #
            project_name = str(edit_project_form.name.data)
            if project is not None:
                project.name = project_name
                db.session.commit()
                #
                flash("Project "+project_name+" updated successfully!", 'success')     
        #
        edit_project_form.name.data = project.name
        #    
        return render_template('edit_project.html', title='Project', form = edit_project_form, project=project)
    else:
        flash("Project could not be found",'danger')
        return redirect(url_for('home'))

@projects_domain.route("/add_worker_to_project", methods=['POST'])
@login_required
def add_worker_to_project():
    project_id = int(request.form.get('project_id', -1))
    person_id = int(request.form.get('employee_id', -1))
    #
    if project_id != -1 and person_id != -1:
        worker_role = Role.query.filter_by(name='worker').first()
        employment = Employment(person_id=person_id, project_id=project_id, role=worker_role)
        db.session.add(employment)
        db.session.commit()
    #
    flash("Worker added to project!", 'success')     
    #
    return redirect(url_for('projects_domain.projects'))

@projects_domain.route("/remove_worker_from_project", methods=['POST'])
@login_required
def remove_worker_from_project():
    project_id = int(request.form.get('project_id', -1))
    person_id = int(request.form.get('person_id', -1))
    #
    if project_id != -1 and person_id != -1:
        worker_role = Role.query.filter_by(name='worker').first()
        Employment.query.filter_by(project_id=project_id ).filter_by(person_id=person_id).filter_by(role_id=worker_role.id).delete()
        db.session.commit()
    #
    flash("Worker removed from project!", 'success')     
    #
    return redirect(url_for('projects_domain.projects'))
    

@projects_domain.route("/add_manager_to_project", methods=['POST'])
@login_required
def add_manager_to_project():
    project_id = int(request.form.get('project_id', -1))
    manager_id = int(request.form.get('manager_id', -1))
    #
    if project_id != -1 and manager_id != -1:
        worker_role = Role.query.filter_by(name='manager').first()
        employment = Employment(person_id=manager_id, project_id=project_id, role=worker_role)
        db.session.add(employment)
        db.session.commit()
    #
    flash("Manager added to project!", 'success')     
    #
    return redirect(url_for('projects_domain.projects'))


@projects_domain.route("/remove_manager_from_project", methods=['POST'])
@login_required
def remove_manager_from_project():
    project_id = int(request.form.get('project_id', -1))
    person_id = int(request.form.get('person_id', -1))
    #
    if project_id != -1 and person_id != -1:
        worker_role = Role.query.filter_by(name='manager').first()
        Employment.query.filter_by(project_id=project_id ).filter_by(person_id=person_id).filter_by(role_id=worker_role.id).delete()
        db.session.commit()
    #
    flash("Manager removed from project!", 'success')     
    #
    return redirect(url_for('projects_domain.projects'))


@projects_domain.route("/delete_project", methods=['POST'])
@login_required
def delete_project():
    project_id = int(request.form.get('project_id', -1))
    #
    if project_id != -1:
        Project.query.filter_by(id=project_id).delete()
        db.session.commit()
        #
        flash("Project deleted!", 'success')     
    #
    return redirect(url_for('projects_domain.projects'))
#=============================================================================================================================

@projects_domain.route("/generate_project_report", methods=['POST'])
@login_required
def generate_project_report():
    project_id = int(request.form.get('project_id', -1))
    if project_id == -1:
        flash("Project could not be found",'danger')
        return redirect(url_for('home'))
    #    
    project = Project.query.filter_by(id=project_id).first()
    #
    report_folder = os.path.join(app.root_path, 'static/reports')
    image_folder = os.path.join(app.root_path, 'static/profile_pics')
    report_location = generate_report(report_folder, project.name, project.name, project.posts, image_folder)
    #
    return send_file(report_location, as_attachment=True)