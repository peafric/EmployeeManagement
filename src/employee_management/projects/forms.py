from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField
from wtforms.validators import DataRequired, Length

#=============================================================================================================================
class CreateProjectForm(FlaskForm):
    name = StringField("Project name", validators=[DataRequired(), Length(min=2, max=20)])
    #
    submit = SubmitField('Create')

class EditProjectForm(FlaskForm):
    name = StringField("Project name", validators=[DataRequired(), Length(min=2, max=20)])
    #
    submit = SubmitField('Save')
#=============================================================================================================================
