import time
from reportlab.lib.enums import TA_LEFT
from reportlab.lib.pagesizes import letter
from reportlab.platypus import SimpleDocTemplate, Paragraph, Spacer, Image, Table
from reportlab.lib.styles import getSampleStyleSheet
from reportlab.lib.units import inch
#
import os

def generate_report(report_folder, report_name, project_name, posts, image_folder):
    doc_location = os.path.join(report_folder,report_name+".pdf")
    doc = SimpleDocTemplate(doc_location,pagesize=letter, rightMargin=72,leftMargin=50,topMargin=72,bottomMargin=18)
    Story=[]

    #Add style        
    styles=getSampleStyleSheet()
    
    #Write the time of writing this document
    ptext = '<font size="12">Project: <b>'+ project_name + '</b> <br/> Document creation date: '+time.ctime()+'</font>'
    Story.append(Paragraph(ptext, styles["Normal"]))

    #Add space
    Story.append(Spacer(1, 12))
    
    #Add posts    
    for post in reversed(posts):
        im = Image(os.path.join(image_folder,post.author.image_file), 0.3*inch, 0.3*inch)
        #
        ptext = '<font size="12"> <b><i> Author and date: </i></b>' + post.author.name + ' ' + post.date.strftime("%d.%m.%Y") + '</font>'
        author_paragraph = Paragraph(ptext, styles["Normal"])
        #
        ptext = '<font size="12"> <b><i> Content: </i></b> <br/>' + post.content + '</font>'
        content_paragraph = Paragraph(ptext, styles["Normal"])
        #
        ptext = '<font size="12"> <b><i> Duration: </i></b>' + str(post.duration) + 'h</font>'
        duration_paragraph = Paragraph(ptext, styles["Normal"])
        #        
        tbl_data = [
            [Paragraph('', styles["Normal"]), Paragraph('', styles["Normal"]), author_paragraph],
            [Paragraph('', styles["Normal"]), im, content_paragraph],
            [Paragraph('', styles["Normal"]), Paragraph('', styles["Normal"]), duration_paragraph],
        ]
        #
        tbl = Table(tbl_data, colWidths=[0.8*inch, 0.8*inch, 6.7*inch])
        Story.append(tbl)
        #
        Story.append(Spacer(1, 36))

    doc.build(Story)
    #
    return doc_location