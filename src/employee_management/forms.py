from flask_wtf import FlaskForm
from wtforms import StringField, SubmitField, TextAreaField
from wtforms.validators import DataRequired, Length, Email

#=============================================================================================================================
class ContactForm(FlaskForm):
    message = TextAreaField("Message", validators=[DataRequired(), Length(min=2, max=1000)])
    email = StringField('Email', validators=[DataRequired(), Email()])
    submit = SubmitField('Send')
#=============================================================================================================================

