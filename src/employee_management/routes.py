from employee_management import app, db, socketio
#
from employee_management.posts.forms import PostForm
from employee_management.forms import ContactForm
#
from flask import render_template, flash, url_for, jsonify
from flask import request
#
from flask_login import current_user, login_required
#
from employee_management.models import Post
#
from datetime import datetime
    
#=============================================================================================================================

POST_BATCH = 10

def encode_post_data(post):
    return {
            'person_name':post.author.name,
            'project_name':post.project.name,
            'date':post.date.strftime("%d.%m.%Y %H:%M"),
            'content':post.content,
            'duration':post.duration,
            'image_file': url_for('static', filename='profile_pics/' + post.author.image_file)
        }

@app.route("/", methods=['GET', 'POST'])
@app.route("/home", methods=['GET', 'POST'])
@login_required
def home():

    #Check for form mistakes
    form = PostForm()
    form.project.choices = [(employment.project.id,employment.project.name) for employment in current_user.employments if employment.role.name=="worker"]
    if form.validate_on_submit():
        post_text = form.content.data
        post_project_id = form.project.data
        post_duration = form.duration.data if form.duration.data is not None else 0
        #
        post = Post(person_id=current_user.id, project_id=post_project_id, content=post_text, duration=post_duration)
        db.session.add(post)
        db.session.commit()
        #
        post_data = encode_post_data(post)
        socketio.emit('new_post', post_data);
        #
        flash("Posted successfully!", 'success') 
    
    #Is worker ?
    is_worker = len(form.project.choices)!=0
        
    #Get posts
    posts = [ post for employment in current_user.employments for post in employment.project.posts if employment.role.name == 'manager']
    posts.sort(key=lambda x:x.date)
    posts.reverse()
    if len(posts) > POST_BATCH:
        posts = posts[:10]
        more_posts = True
    else:
        more_posts = False
    
    last_post_date =  (datetime.now() if len(posts) == 0 else posts[-1].date).strftime("%d.%m.%Y %H:%M:%S")
        
    #Is manager ? 
    is_manager = False
    for employment in current_user.employments:
        if employment.role.name == 'manager':
            is_manager = True
            break
        
    # 
    return render_template('home.html', title="Home", is_worker=is_worker, is_manager=is_manager, posts=posts, more_posts=more_posts, last_post_date=last_post_date, post_form=form)  


@app.route("/get_older_posts", methods=['POST'])
@login_required
def get_older_posts():
    date_string = request.form.get('last_date')
    if date_string is None:
        return jsonify([])
    date = datetime.strptime(date_string, "%d.%m.%Y %H:%M:%S")
    #
    posts = []
    for post in current_user.posts:
        if post.date < date:
            posts.append(post)        
    #
    posts.sort(key=lambda x:x.date)
    posts.reverse()
    #
    if len(posts) > POST_BATCH:
        posts = posts[:10]
        more_posts = True
    else:
        more_posts = False    
    post_data = [encode_post_data(post) for post in posts]
    #
    last_post_date = (date if len(posts) == 0 else posts[-1].date).strftime("%d.%m.%Y %H:%M:%S")
    #
    response_data = {
        "posts": post_data,
        "last_post_date": last_post_date,
        "more_posts": more_posts,
    }
    #
    return jsonify(response_data)
    

#=============================================================================================================================
@app.route("/about")
def about():    
    return render_template('about.html',title='About')

@app.route("/contact", methods=['GET','POST'])
def contact():    
    form = ContactForm()
    if form.validate_on_submit():
        #
        #TODO: implement
        #
        flash("Message sent successfully!", 'success') 
        
    return render_template('contact.html',title='Contact', form=form)
#=============================================================================================================================
