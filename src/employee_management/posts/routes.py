from employee_management import db
#
from employee_management.posts.forms import EditPostForm
#
from flask import Blueprint
from flask import render_template, flash, redirect, url_for
from flask import request
#
from flask_login import current_user, login_required
#
from employee_management.models import Post
    
#=============================================================================================================================
posts_domain = Blueprint('posts_domain', __name__)    
#=============================================================================================================================
@posts_domain.route("/delete_post", methods=['POST'])
@login_required
def delete_post():
    post_id = int(request.form.get('post_id', -1))
    posts_which_can_be_deleted = set([post.id for post in current_user.posts])
    #
    if post_id != -1 and post_id in posts_which_can_be_deleted:
        Post.query.filter_by(id=post_id).delete()
        db.session.commit()
        #
        flash("Post deleted!", 'success')     
    #
    return redirect(url_for('people_domain.profile',person_id=current_user.id))

@posts_domain.route("/edit_post", methods=['GET', 'POST'])
@login_required
def edit_post():
    post_id = int(request.args.get('post_id', -1))
    posts_which_can_be_edited = set([post.id for post in current_user.posts])
    if post_id != -1 and post_id in posts_which_can_be_edited:
        post = Post.query.filter_by(id=post_id).first()
        #
        form = EditPostForm()
        #
        if form.validate_on_submit():
            post_text = form.content.data
            post_duration = int(form.duration.data if form.duration.data is not None else 0)
            #
            post.content = post_text
            post.duration = post_duration
            db.session.commit()
            #
            flash("Post edited successfully!", 'success') 
        #
        form.content.data = post.content
        form.duration.data = post.duration
        #            
        return render_template('post.html',title='Post',form=form)
    #
    flash("Post could not be found!", 'danger') 
    return redirect(url_for('home'))
#=============================================================================================================================
