from flask_wtf import FlaskForm
from wtforms import SubmitField, TextAreaField, IntegerField, SelectField
from wtforms.validators import DataRequired, Length, NumberRange, Optional

#=============================================================================================================================
class PostForm(FlaskForm):
    content = TextAreaField("Content", validators=[DataRequired(), Length(min=2, max=1000)])
    project = SelectField("Project", validators=[DataRequired()])
    duration = IntegerField("Duration", validators=[Optional(), NumberRange(min=0, max=24)] )
    #
    submit = SubmitField('Post')

class EditPostForm(FlaskForm):
    content = TextAreaField("Content", validators=[DataRequired(), Length(min=2, max=1000)])
    duration = IntegerField("Duration", validators=[Optional(), NumberRange(min=0, max=24)] )
    #
    submit = SubmitField('Save')
#=============================================================================================================================
