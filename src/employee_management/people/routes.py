from employee_management import app, db
#
from employee_management.people.forms import RegistrationForm, LoginForm, EditProfileForm
#
from flask import Blueprint
from flask import render_template, flash, redirect, url_for
from flask import request
#
from flask_login import login_user, current_user, logout_user, login_required
#
from employee_management.models import Person
#
import os
import secrets
from PIL import Image
#
from datetime import datetime, timedelta

#=============================================================================================================================
people_domain = Blueprint('people_domain', __name__)    
#=============================================================================================================================
@people_domain.route("/register", methods=['GET', 'POST'])
def register():
    form = RegistrationForm()
    if form.validate_on_submit():
        #
        person = Person(name=form.name.data,email=form.email.data,username=form.username.data,password=form.password.data)
        db.session.add(person)
        db.session.commit()
        #
        flash("Account created for "+str(form.username.data)+"!", 'success')
        #
        return redirect(url_for('people_domain.login'))
    
    return render_template('register.html', title='Register', form=form)

@people_domain.route("/login", methods=['GET', 'POST'])
def login():
    if current_user.is_authenticated:
        return redirect(url_for('home'))
    form = LoginForm()
    if form.validate_on_submit():
        user = Person.query.filter_by(username=form.username.data).first()
        if user and user.password==form.password.data:
            login_user(user, remember=form.remember.data)
            next_page = request.args.get('next')
            return redirect(next_page) if next_page else redirect(url_for('home'))
        else:
            flash('Login Unsuccessful. Please check email and password', 'danger')
    return render_template('login.html', title='Login', form=form)

@people_domain.route("/logout")
@login_required
def logout():
    logout_user()
    return redirect(url_for('home'))
#=============================================================================================================================
@people_domain.route("/profile",methods=['GET','POST'])
@login_required
def profile():    
    person_id = int(request.args.get('person_id',-1))
    if person_id == -1:
        flash("Could not find the requested profile","danger")
        return redirect(url_for('home'))
    #
    user = Person.query.filter_by(id=person_id).first()
    #
    form = EditProfileForm()
    if form.validate_on_submit():
        #
        if form.picture.data:
            picture_file = save_picture(request.files[form.picture.name])
            current_user.image_file = picture_file
        current_user.name = form.name.data
        current_user.email = form.email.data
        db.session.commit()
        #
        flash("Profile adjusted for "+current_user.name+"!", 'success')
    #
    form.name.data = current_user.name
    form.email.data = current_user.email
    #
    posts = user.posts
    posts.sort(key= lambda p:p.date)
    posts.reverse()
    #
    image_file = url_for('static', filename='profile_pics/' + user.image_file)
    #
    colleagues = set()
    if len(user.employments)!=0:
        for employement in user.employments:
            for personnel_data in employement.project.personnel:
                if personnel_data.person.id != user.id:
                    colleagues.add(personnel_data.person)
        colleagues = list(colleagues)
        colleagues.sort(key=lambda c:c.name)
    #
    person_projects = None
    day_data_list = None
    if len(posts)!=0:
        #
        today = datetime.now()
        day_data = dict()
        for i in range(30):
            day = today - timedelta(days=i)
            #
            key = day.strftime("%d.%m.%Y")
            #
            day_data[key] = dict()
            day_data[key]['hours'] = sum([post.duration for post in posts if post.date.date()==day.date()])
            day_data[key]['posts'] = sum([1 for post in posts if post.date.date()==day.date()])
            #
        day_data_list = []
        for day_id in day_data:
            day_data_list.append((day_id,day_data[day_id]['hours'],day_data[day_id]['posts']))
        day_data_list.reverse()    
        #
        person_projects = []
        for employement in user.employments:
            if employement.role.name=='worker':
                user_hours = sum([post.duration for post in user.posts if post.project_id==employement.project_id])
                user_posts = sum([1 for post in user.posts if post.project_id==employement.project_id])
                person_projects.append((employement.project.name,user_hours,user_posts))
    #
    if person_projects is None and day_data_list is None:
        if len(colleagues)!=0:
            return render_template('profile.html',title='Profile',user=user, image_file=image_file, form=form, posts=posts, colleagues=colleagues)
        else:
            return render_template('profile.html',title='Profile',user=user, image_file=image_file, form=form, posts=posts)
    else:
        if len(colleagues)!=0:
            return render_template('profile.html',title='Profile',user=user, image_file=image_file, form=form, posts=posts, person_projects=person_projects, day_data=day_data_list, colleagues=colleagues)
        else:
            return render_template('profile.html',title='Profile',user=user, image_file=image_file, form=form, posts=posts, person_projects=person_projects, day_data=day_data_list)
    
def save_picture(form_picture):
    random_hex = secrets.token_hex(8)
    _, f_ext = os.path.splitext(form_picture.filename)
    picture_fn = random_hex + f_ext
    picture_path = os.path.join(app.root_path, 'static/profile_pics', picture_fn)

    output_size = (125, 125)
    i = Image.open(form_picture)
    i.thumbnail(output_size)
    i.save(picture_path)

    return picture_fn
