from flask import Flask
#
from flask_sqlalchemy import SQLAlchemy
#
from flask_login import LoginManager
#
from flask_socketio import SocketIO
#
app = Flask(__name__)
app.config['SECRET_KEY'] = '5791628bb0b13ce0c676dfde280ba245'
app.config['SQLALCHEMY_DATABASE_URI'] = 'sqlite:///../../data/site.db'
#
db = SQLAlchemy(app)
#
login_manager = LoginManager(app)
login_manager.login_view = 'people_domain.login'
login_manager.login_message_category = 'info'
#
socketio = SocketIO(app)

#Needed to initialize the routes
from employee_management.people.routes import people_domain
from employee_management.projects.routes import projects_domain
from employee_management.posts.routes import posts_domain
app.register_blueprint(people_domain)
app.register_blueprint(projects_domain)
app.register_blueprint(posts_domain)
    
from employee_management import routes 
