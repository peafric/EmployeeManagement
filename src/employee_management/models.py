from employee_management import db
from employee_management import login_manager
#
from flask_login import UserMixin
#
from sqlalchemy.orm import backref
#
from datetime import datetime


#=================================================================================================================
@login_manager.user_loader
def load_user(user_id):
    return Person.query.get(int(user_id))
#=================================================================================================================
from sqlalchemy.engine import Engine
from sqlalchemy import event

@event.listens_for(Engine, "connect")
def set_sqlite_pragma(dbapi_connection, connection_record):
    cursor = dbapi_connection.cursor()
    cursor.execute("PRAGMA foreign_keys=ON")
    cursor.close()    
#=================================================================================================================
class Role(db.Model):
    id  = db.Column(db.Integer, primary_key=True)
    #
    name = db.Column(db.String(60), nullable=False)

    def __repr__(self):
        return "Project(" + str(self.name) + ")"
#=================================================================================================================
class Employment(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    person_id = db.Column(db.Integer, db.ForeignKey("person.id", ondelete="cascade"), nullable=False)
    project_id = db.Column(db.Integer, db.ForeignKey("project.id", ondelete="cascade"), nullable=False)
    role_id = db.Column(db.Integer, db.ForeignKey("role.id", ondelete="cascade"), nullable=False)
    #
    __table_args__ = (db.UniqueConstraint(person_id, project_id, role_id),)
    #
    person = db.relationship("Person", back_populates="employments")
    project = db.relationship("Project", back_populates="personnel")
    role = db.relationship("Role")

    def __repr__(self):
        return "Employment(" + str(self.person.name) + ", " + str(self.project.name) + ", " + str(self.role.name) + ")"    
#=================================================================================================================
class Person(db.Model,UserMixin):
    id = db.Column(db.Integer, primary_key=True)
    #
    username = db.Column(db.String(20), unique=True, nullable=False)
    password = db.Column(db.String(60), nullable=False)
    #
    name = db.Column(db.String(60), nullable=False)
    email = db.Column(db.String(120), unique=True, nullable=False)
    image_file = db.Column(db.String(20), nullable=False, default='default.jpg')
    #
    posts = db.relationship('Post', backref="author", lazy=True)
    #
    employments = db.relationship("Employment", cascade="all, delete", back_populates="person")
    
    def __repr__(self):
        return "Person(" + str(self.username) + ", "+ str(self.email) + ", " + str(self.name) + ")"
#=================================================================================================================
class Project(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    #
    name = db.Column(db.String(60), nullable=False)
    #
    posts = db.relationship('Post', backref="project", cascade="all, delete", lazy=True)
    #
    personnel = db.relationship("Employment", back_populates="project")

    def __repr__(self):
        return "Project(" + str(self.name) + ")"    
#=================================================================================================================
class Post(db.Model):
    id = db.Column(db.Integer, primary_key=True)
    #
    person_id = db.Column(db.Integer, db.ForeignKey('person.id', ondelete="cascade"), nullable=False)
    project_id = db.Column(db.Integer, db.ForeignKey('project.id', ondelete="cascade"), nullable=False)
    #
    date = db.Column(db.DateTime, nullable=False, default=datetime.utcnow)
    content = db.Column(db.Text, nullable=False)
    duration = db.Column(db.Integer, nullable=False, default=0)

    def __repr__(self):
        return "Post( Date: " + str(self.date) + ", Person: " + str(self.person_id) + ", Project: " + str(self.project_id) + ")"