# EmployeeManagement

Web application for employee time management.

## Home page ##
Functionality:
* Post project progress messages which include a message and optional task duration information.
* View posts from your employees (people you manage on projects).
![Home](./readme_images/home.png)


## Profile page ##
Functionality:
* Adjust your profile information
* View your statistics. 
* Edit your posts. 
* Delete your posts. 
![Profile](./readme_images/profile.png)
![Profile2](./readme_images/profile2.png)


## Projects page ##
Functionality:
* Manage managers and workers on existing projects
* Create new projects
* Edit existing projects (project name)
![Projects](./readme_images/projects.png)

## Project page ##
Functionality:
* View project statistics.
* Export project information (posts) in a .pdf report. 
* View project related posts.
![Project](./readme_images/project.png)

